---
Week: 22
Content: Project part 2 phase 3
Material: See links in weekly plan
Initials: MON/NISI
---

# Week 22

This is the first week of phase 3 `make it useful for the user`

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
None from the teachers at this time.

### Learning goals
None from the teachers at this time.

## Deliverables
* Mandatory weeky meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
    5. Any other business (AOB)

## Schedule

Monday

* 8:15 Introduction to the day, test exam Q/A session
    
    - See message on itslearning for exam order and agenda
    - Q&A list is on [hackmd.io](https://hackmd.io/29oDMYmEQ5i1hfsrL1D98g)


* 9:00? You work

    

Tuesday

* 8:15 Test exam
    - Test exam occupies half a day, this means you have half a day for the project.


## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19s-itt2-project/19S_ITT2_exercises.pdf) for details, if applicable.


## Comments
* slides about the CLA23 are [here](https://hackmd.io/p/rJip6bxaN#/)