---
Week: 21
Content: Project part 2 phase 2
Material: See links in weekly plan
Initials: MON/NISI
---

# Week 21

This is the second week of phase 2 `consolidation`

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
None from the teachers at this time.

### Learning goals
None from the teachers at this time.

## Deliverables
* Mandatory weeky meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
    5. Any other business (AOB)

* OLA22 (compulsory learning activity) You have received an email from wiseflow

## Schedule

Monday

* 8:15 Introduction to the day, general Q/A session

* 08:45? Project experience exchange
    
    See week 18 for agenda.

    This is now optional, if you find it relevant please attend.

* 9:30? You work

    Group morning meeting
    
        You will meet in your groups every morning and decide on who does what. This is part of the project management, and is mandatory.
        Ordinary agenda:
        1. (5 min) Round the table: What did I do, and what did I finish?
        2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
        3. (5 min) Round the table: Claim one task each.

    Remember to come ask questions if you have any.  



Tuesday

* 8:15 Introduction to the day, general Q/A session
    - Q&A list is on [hackmd.io](https://hackmd.io/29oDMYmEQ5i1hfsrL1D98g)

* 8:30? Course evaluation action plan

    In class, we will review the course evaluation and decide the course of action

    Link to [report A class](http://www.survey-xact.dk/report/shared/74939614-ee00-4f14-9ff7-9235ee83470c)

    Link to [report B class](http://www.survey-xact.dk/report/shared/f0a81630-fb59-4329-a276-74f7a6f3d9b9)

* 9:00? CLA23 evaluation 

* 9:30? You work

    Group morning meeting

        Same agenda as monday
        
    Remember to come ask questions if you have any. 

* 9:30? Teacher meetings

    Timeslot for you weekly 10 min. meeting with the teachers.

    Remember to book a time and have an agenda prepared. 