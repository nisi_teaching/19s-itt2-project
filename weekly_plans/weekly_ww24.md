---
Week: 24
Content: Project part 2 phase 3
Material: See links in weekly plan
Initials: MON/NISI
---

# Week 24

This is the last project week

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
None from the teachers at this time.

### Learning goals
None from the teachers at this time.

## Deliverables
* Elevator pitch about the project part 2 - maximum 3 minutes!
* Demonstration of the project part 2 - maximum 3 minutes!

## Schedule

Monday

* Whit Monday - no lectures planned

Tuesday

* 8:15 Introduction to the day
    - Short presentation and runthrough of the demoday expectations 
    - Reminder about the Q&A session in the afternoon - please put your questions in the Q&A list on [hackmd.io](https://hackmd.io/29oDMYmEQ5i1hfsrL1D98g)

* 8:30? Demoday preparation and setup
    * Demoday will take place at the network lab
    * Prepare an [elevator pitch](https://www.mindtools.com/pages/article/elevator-pitch.htm) for your product idea
    * Prepare a demo of what your product can do - make sure that everything works and have a plan on how to demonstrate it.

* 10:00 Demoday

    * Demonstration of the project part 2 result.  
        * As a minimum you have to present your elevator pitch and demo of the product to the teachers.   
        * You have to remain at your booth for the entire timespan and give your pitch + demo to all visitors.

* 12:15 Q&A session

    This is a session where we answer questions regarding the project and exam recap questions.

    We will use the Q&A list on [hackmd.io](https://hackmd.io/29oDMYmEQ5i1hfsrL1D98g) to gather questions and answers

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19s-itt2-project/19S_ITT2_exercises.pdf) for details, if applicable.


## Comments

we only have one day this week because of Whit Monday