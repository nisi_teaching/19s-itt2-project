---
Week: 14
Content: Project part 1 phase 3
Material: See links in weekly plan
Initials: MON/NISI
---

# Week 14 Make it useful for the user

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* All groups have participated in the product test
* All groups have a public gitlab project for the minimum system
* All students has a link to the public gitlab project included in their personal Gitlab pages portfolio.


### Learning goals

### Product test

* level 1: The student can present a working minimum system.
* level 2: The student can present the minimum system and explain the different parts and how they are implemented.
* level 3: The student is able to present the minimal system and can reflect on future possibilities for the system. 

## Deliverables

* Compolsory learning activity - Product test
* Personal Gitlab pages portfolio
* Minimum system public and included in portfolio
* Written evaluation on the project plan

## Schedule

Tuesday

* 8:15 Introdution to the day, general Q/A session

* 8:30 You work on deliverables

    Come ask us if you have questions.

* 12:30 Product test in network lab

Wednesday

* 8:15 Group morning meeting

    You will meet in your groups every morning and decide on who does what. This is part of the project management, and is mandatory.
    Ordinary agenda:
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.

* 9:00 You work on deliverables

## Hands-on time

### Project evaluation

Look through your project plan from the beginning of the project, and evaluate.

* Did you fulfill the goals and deliverables as they are stated in the plan? 
  * anything new happen? anything not done?
* You defined risks in the project and mitigating controls
  * how well did you predict problems?
  * were the control sufficient?
* Did you follow your communication plan?
* and anything else that comes to mind when you comapre the plan to what actually happened.

Write a summary and conclusions and put in on gitlab.

## other exercises, if applicable

See the [exercise document](https://eal-itt.gitlab.io/19s-itt2-project/19S_ITT2_exercises.pdf) for details.

## Comments

* Teachers are attending a meeting the entire Wednesday and will be unavailable.
* Remember to mention that we have added some openscad links to week 11
