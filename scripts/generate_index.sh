#!/usr/bin/env bash

if [ "x" == "x$1" ]; then
	echo "usage: $0 <filelist or wildcard>"
	exit 1
fi

echo Generating html from file list: $@ > /dev/stderr

cat << EndOfMessage
<!DOCTYPE html>
<html lang="en">
<head>
  	<meta charset="UTF-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<meta http-equiv="X-UA-Compatible" content="ie=edge">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<title>19s-itt2-project files</title>
</head>
<body>
	<div class="container-fluid">
    <div class="row">
        <h1 class="col-sm-12">Files available</h1>
    </div>
	<div class="btn-group-vertical">
EndOfMessage

for F in $@; do
	BASE=`basename "$F"`
	echo "	<a class=\"btn btn-primary btn-lg\" href=\"$BASE\">$F</a>"
done

cat << EndOfMessage
	</div>
</div>
</body>
</html>
EndOfMessage
