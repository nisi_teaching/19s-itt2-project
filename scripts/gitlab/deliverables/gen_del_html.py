#!/usr/bin/env python3

from deliverables import Deliverables
from utils import get_token, get_filename_from_argv
import gitlab
import jinja2
import sys

html_filename = "deliverables.html"


def render(deliverables, templatefilename='deliverables.html.j2'):
    # environment = jinja2.Environment(loader=jinja2.FileSystemLoader(script_path))
    file_loader = jinja2.FileSystemLoader('./')
    environment = jinja2.Environment(loader=file_loader)
    ret_str = environment.get_template(templatefilename).render(projects=deliverables)
    return ret_str


def save_to_file(text, filename=html_filename):
    with open(filename, "w", encoding="utf8") as fh:
        fh.write(text)


if __name__ == "__main__":
    # gitlab init
    token = get_token('token.txt')
    try:
        gl = gitlab.Gitlab('https://gitlab.com', private_token=token)
        gl.auth()
    except gitlab.exceptions.GitlabError as e:
        print("Error: {}".format(e))
        print("404 could mean bad token...")
        exit(1)

    sys.stdout.write("Authenticated to gilab\n")

    list_of_groups_filename = get_filename_from_argv(default_filename='../../../groups.txt')
    deliverables = []

    sys.stdout.write("Reading group info from {}\n".format(list_of_groups_filename))
    with open(list_of_groups_filename) as gr_lst:
        for project_name in gr_lst:
            sys.stdout.write("- Reading group: {}\n".format(project_name.strip()))
            d = Deliverables(gl, project_name)
            deliverables.append(d)

    # and output to stdout
    sys.stdout.write("Rendering page\n")
    output_text = render(deliverables)

    sys.stdout.write("Saving page to {}\n".format(html_filename))
    save_to_file(output_text, html_filename)
