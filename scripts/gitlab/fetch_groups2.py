#!/usr/bin/env python

import gitlab
import sys
import jinja2
import yaml

groupfile='group.yml'
default_groups_filename = 'groups_part2.txt'
default_template_filename = 'groups2.md.j2'

def get_token( filename='token.txt'):
	with open(filename) as f:
		token=f.readline()
	return token.strip()

def convert_to_md( yml_dict ):
	templateLoader = jinja2.FileSystemLoader(searchpath="./")
	templateEnv = jinja2.Environment(loader=templateLoader)
	template_filename = default_template_filename
	template = templateEnv.get_template(template_filename)
	return template.render( yml_dict )

def output_from_file( filename ):
	config = read_yaml( filename )
	print( convert_to_md( config ) )

def read_yaml( filename ):
	with open( filename ) as f:
		config = yaml.safe_load( f )
		return config

def output_from_gitlab( group_list):
	token = get_token()

	gl = gitlab.Gitlab('https://gitlab.com', private_token=token )
	gl.auth()

	for p_name in group_list:
		try:
			project = gl.projects.get(p_name.strip())

			f = project.files.get(file_path=groupfile, ref='master')

			group_data = yaml.safe_load( f.decode() )
			group_data['project_path'] = "[{}]({})".format( project.web_url, project.web_url )

			print( convert_to_md( group_data ) )

		except gitlab.exceptions.GitlabGetError:
			print("Unknown group")
			print("----------------" )
			print("Unable to fetch file {} from {}".format(groupfile, p_name.strip() ) )
			print( "")
		except TypeError:
			#yes silently failing ...
			pass

if __name__ == "__main__":

	if len(sys.argv) > 1:
		list_of_groups_filename = sys.argv[1]
	else:
		list_of_groups_filename = default_groups_filename

	if len(sys.argv) > 2:
		output_from_file( sys.argv[2] )
	else:
		with open(list_of_groups_filename) as gr_lst:
			output_from_gitlab( gr_lst.readlines())
