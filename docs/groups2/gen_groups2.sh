#!/usr/bin/env bash

set -e

cat group2.md.prepend


if ! ../../scripts/gitlab/fetch_groups2.py ../../groups_part2.txt; then
  exit $?
fi


cat group2.md.append
