---
title: '19S ITT2 Project'
subtitle: 'OLA23 project reflections'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Morten Bo Nielsen \<mon@ucl.dk\>']
main_author: 'Morten Bo Nielsen'
date: \today
email: 'mon@ucl.dk'
left-header: \today
right-header: OLA23
skip-toc: true
---

Introduction
=====

The compulsory learning activity 23 is about projects and your role in them. Handin is schedule for week 19 2019.


Content
===========

The hand-in is a pdf handed in on wiseflow. It will contain the following:

* A description of your own contribution to the project done in part 1
    * What was you main tasks? 
    * How did you structure your work? use examples
    * How did you communicate and resolve conflicts in the team? use examples
    * How did you document your work? use examples.
    
* In appendix, include a list from gitlab with your personal issues and commit history.
    * yes, this will be long for some of you.

We expect 3-4 pages + appendix. Note that this is an individual hand-in.

Follow-up
============

After hand-in, the teacher will review the documents, and we will make a session on class where we sum up the hightlights.